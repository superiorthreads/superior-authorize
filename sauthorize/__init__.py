import xml.etree.ElementTree as E

from sauthorize.configuration import Configuration
from sauthorize.address import Address
from sauthorize.bank_account import BankAccount
from sauthorize.batch import Batch
from sauthorize.credit_card import CreditCard
from sauthorize.customer import Customer
from sauthorize.environment import Environment
from sauthorize.exceptions import AuthorizeError
from sauthorize.exceptions import AuthorizeConnectionError
from sauthorize.exceptions import AuthorizeResponseError
from sauthorize.exceptions import AuthorizeInvalidError
from sauthorize.recurring import Recurring
from sauthorize.transaction import Transaction


# Monkeypatch the ElementTree module so that we can use CDATA element types
E._original_serialize_xml = E._serialize_xml
def _serialize_xml(write, elem, *args, **kwargs):
    if elem.tag == '![CDATA[':
        write('<![CDATA[%s]]>' % elem.text)
        return
    return E._original_serialize_xml(write, elem, *args, **kwargs)
E._serialize_xml = E._serialize['xml'] = _serialize_xml
